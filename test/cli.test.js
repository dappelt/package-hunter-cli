/* eslint-env mocha */
'use strict'

const sinon = require('sinon')
const assert = require('assert')

const createCLI = require('../cli')

describe('CLI Test', function () {
  describe('passes config params to the client', function () {
    beforeEach(function () {
      this.conf = {
        HOST: 'https://example.com',
        USER: 'alice',
        PASS: 'password'
      }

      this.client = sinon.stub().returns({ analyze: () => {} })
      this.program = createCLI(this.conf, this.client)
    })

    it('sets the host', function () {
      this.program.parse([null, 'cli.js', 'analyze', 'some-dependency.tgz'])
      assert(this.client.calledOnce)
      assert(this.client.calledWith(this.conf.HOST))
    })

    it('sets user and pass', function () {
      this.program.parse([null, 'cli.js', 'analyze', 'some-dependency.tgz'])
      assert(this.client.calledOnce)
      const expectedOpts = { format: 'raw', user: this.conf.USER, pass: this.conf.PASS }
      const actualOpts = this.client.args[0][1] // get the opts object. It should contain user and pass
      assert.deepStrictEqual(actualOpts, expectedOpts)
    })
  })
})
