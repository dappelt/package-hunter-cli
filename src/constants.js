module.exports = {
  PACKAGE_MANAGERS: {
    NPM: 'npm',
    YARN: 'yarn',
    BUNDLER: 'bundler'
  }
}
