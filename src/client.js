'use strict'

const got = require('got')
const fs = require('fs')
const url = require('url')
const cliProgress = require('cli-progress')
const debug = require('debug')('pkgs.client')
const Formatter = require('../src/formatter.js')
const { PACKAGE_MANAGERS } = require('../src/constants.js')

class PackageHunterClient {
  constructor (host, opts) {
    // ToDo: validate host is a URL
    this.host = host
    this.user = opts.user
    this.pass = opts.pass
    this.formatter = new Formatter(process.stdout, opts.format)
  }

  async analyzeYarnProject (sources) {
    const path = '/monitor/project/yarn'
    const result = await this._submit(path, sources)
    return result
  }

  async analyzeYarnDependency (sources) {
    const path = '/monitor/dependency/yarn'
    const result = await this._submit(path, sources)
    return result
  }

  async analyze (sources, { packageManager = PACKAGE_MANAGERS.YARN, isProject = true } = {}) {
    if (!Object.values(PACKAGE_MANAGERS).includes(packageManager)) {
      throw new Error('unknown package manager: ' + packageManager)
    }

    const path = `/monitor/${isProject ? 'project' : 'dependency'}/${packageManager}`
    const result = await this._submit(path, sources)
    return result
  }

  async _submit (path, sources) {
    const endpoint = url.resolve(this.host, path)

    debug(`POSTing ${sources} to ${endpoint}`)

    const start_time = this._time()

    const bar = new cliProgress.SingleBar({}, cliProgress.Presets.shades_classic)
    let resp
    try {
      resp = await got.post(endpoint, {
        headers: {
          'content-type': 'application/octet-stream'
        },
        username: this.user,
        password: this.pass,
        body: fs.createReadStream(sources),
        responseType: 'json'
      }).on('uploadProgress', ({ percent, transferred, total }) => {
        if (!bar.isActive) {
          bar.start(total, transferred)
        } else {
          bar.update(transferred)
        }
        if (total === transferred) bar.stop()
      })
    } catch (err) {
      if (err.code === 'ENOENT') {
        console.error(err.message)
      } else {
        console.error(`Error calling ${path}: ${err}`)
        if (err.response) debug('Response: %j', err.response.body)
      }
      throw err
    }

    let result
    const { status, id, reason } = resp.body
    if (status === 'ok') {
      result = await this._poll(id)
    } else {
      debug(`unexpected response: ${status} - ${reason}`)
      throw new Error(reason)
    }

    const end_time = this._time()

    this.formatter.print(result, start_time, end_time)

    return result
  }

  async _poll (id) {
    const searchParams = new URLSearchParams({ id })
    const client = got.extend({ searchParams, responseType: 'json' })

    process.stderr.write('Analyzing')
    let resp = { body: { status: 'pending' } }
    for (; resp.body.status === 'pending'; resp = await client(this.host)) {
      process.stderr.write('.')
      await this._sleep(2000)
    }

    process.stderr.write('\n')
    return resp.body
  }

  async _sleep (ms) {
    return new Promise((resolve) => { setTimeout(resolve, ms) })
  }

  _time () {
    return new Date().toISOString().slice(0,19)
  }
}

module.exports = PackageHunterClient
