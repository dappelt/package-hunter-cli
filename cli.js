#!/usr/bin/env node

const { Command } = require('commander')
const rcConf = require('rc')('package_hunter', {
  // default values
  HOST: 'https://api.package-hunter.xyz'
})

const localPackageMetadata = require('./package.json')
const DefaultPackageHunterClient = require('./src/client')

module.exports = function (conf = rcConf, PackageHunterClient = DefaultPackageHunterClient, packageMetadata = localPackageMetadata) {
  const program = new Command()

  program
    .version(packageMetadata.version)
    .command('analyze <project>')
    .option('-f, --format <raw|gitlab>', 'format the result as "raw", which is the format returned from Package Hunter, or "gitlab", which is expected by GitLab.', 'raw')
    .option('-p, --package', '<project> is a dependency', false)
    .option('-m, --manager <yarn|npm|bundler>', 'the package manager used in <project>', 'yarn')
    .action(async (project, cmdObj) => {
      const client = new PackageHunterClient(conf.HOST, {
        format: cmdObj.format,
        user: conf.USER,
        pass: conf.PASS
      })
      try {
        await client.analyze(project, { packageManager: cmdObj.manager, isProject: !cmdObj.package })
      } catch (err) {
        console.log(err.message)
        process.exit(1)
      }
    })

  return program
}

if (require.main === module) { // we are being called from the terminal
  const program = module.exports()
  program.parse(process.argv)
}
