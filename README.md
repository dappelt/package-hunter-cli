# Package Hunter CLI

Behavorial monitoring for identifying malicious dependencies.

This is the Package Hunter CLI client. It is useful for running Package Hunter jobs in CI pipelines and from your local computer. Requires a running [Package Hunter server](https://gitlab.com/gitlab-org/security-products/package-hunter).

# Getting Started

## GitLab CI

Instructions to add a Package Hunter job to your CI pipeline:
- Set up a Package Hunter server as described in the docs [docs](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/master/README.md#installation).
- Set the CI variables `PACKAGE_HUNTER_USER` and `PACKAGE_HUNTER_PASS` in your GitLab [project settings](https://docs.gitlab.com/ee/ci/variables/) according to your server configuration.
- Include the Package Hunter template in your CI config file `.gitlab-ci.yml`:
```yaml
include: "https://gitlab.com/gitlab-org/security-products/package-hunter-cli/-/raw/main/ci/template/Package-Hunter.gitlab-ci.yml"

.package_hunter-base:
  variables:
    PACKAGE_HUNTER_HOST: "https://package-hunter-server.example"
```

## Locally

### Docker (recommended)

Requirements:
- Docker

```sh
docker run --rm registry.gitlab.com/gitlab-org/security-products/package-hunter-cli analyze --help
```

### From Source

Requirements:
- Node.js v12.16 or newer

```sh
git clone https://gitlab.com/gitlab-org/security-products/package-hunter-cli.git
cd package-hunter-cli
npm install
DEBUG=pkgs* node cli.js analyze --help
```

## Usage

To analyze a project, create an archive with the project sources and pass it to the CLI client:

```sh
tar czvf gitlab.tgz ~/git/gitlab

# using Docker
docker run --rm \
  -v "${PWD}:/usr/src/app" \
  --env "DEBUG=pkgs*" \
  --env "PACKAGE_HUNTER_USER=someuser" --env "PACKAGE_HUNTER_PASS=somepass" \
  registry.gitlab.com/gitlab-org/security-products/package-hunter-cli analyze gitlab.tgz

# using Source
DEBUG=pkgs* PACKAGE_HUNTER_USER=someuser PACKAGE_HUNTER_PASS=somepass node cli.js analyze gitlab.tgz
```

The archive will be send to the Package Hunter server and any suspicious behavior will be reported back. To get an overview of the rules that were violated, you can use jq like so `... analyze gitlab.tgz | jq .result[].rule`.

The Package Hunter server requires authentication. User and password have to be provided to the client via the environemnt variables `PACKAGE_HUNTER_USER` and `PACKAGE_HUNTER_PASS`. You can provide the user that was created [during the installation](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/eaf922f3c02b7c015e1c02b5c979c27f23c8da92/README.md#installation) of the package hunter server or create a new user by running the [create-user](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/eaf922f3c02b7c015e1c02b5c979c27f23c8da92/script/create-user) script.

## Publishing

This project uses semantic versioning. Commits are automatically tagged [based on the commit message](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/2f113a6976b9e8c941a30908095eba2e3bf8b6b4/CONTRIBUTING.md#git-commit-guidelines).

A CI job will build the docker image and publish it to the container registry. Execute the release with:

```sh
# to run release 1.2.3
docker run registry.gitlab.com/gitlab-org/security-products/package-hunter-cli:1.2.3

# or to run the latest release

docker run registry.gitlab.com/gitlab-org/security-products/package-hunter-cli:latest
```

## Contributing

See [CONTRIBUTING.md](./CONTRIBUTING.md).
