FROM node:12.16-buster-slim

# RUN apt-get update && apt-get install -y \
#   git \
#   && rm -rf /var/lib/apt/lists/*

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./
RUN npm ci --only=production

COPY . .

ENTRYPOINT ["node", "cli.js"]
CMD ["--help"]
